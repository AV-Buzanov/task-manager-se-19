package ru.buzanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
@Component
public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[HELP]");
        for (AbstractCommand command : sessionLocator.getCommandsMap()) {
            terminalService.printG(command.command());
            terminalService.printLine(" : ", command.description());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
