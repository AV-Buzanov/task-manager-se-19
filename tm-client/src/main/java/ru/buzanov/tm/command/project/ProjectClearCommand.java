package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
@Component
public class ProjectClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
//        for (@NotNull final ProjectDTO project : projectEndpoint.findAllP(sessionLocator.getCurrentSession()))
//            taskEndpoint.removeByProjectIdT(sessionLocator.getCurrentSession(), project.getId());
        projectEndpoint.removeAllP(sessionLocator.getCurrentSession());
        terminalService.printLineG("[ALL PROJECTS REMOVED WITH CONNECTED TASKS]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
