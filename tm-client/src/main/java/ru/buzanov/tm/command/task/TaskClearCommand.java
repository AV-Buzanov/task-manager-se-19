package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.SessionDTO;
@Component
public class TaskClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        taskEndpoint.removeAllT(session);
        terminalService.printLineG("[ALL TASKS REMOVED]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
