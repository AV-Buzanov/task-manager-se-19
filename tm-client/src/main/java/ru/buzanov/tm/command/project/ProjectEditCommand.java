package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.ProjectDTO;
import ru.buzanov.tm.endpoint.SessionDTO;
@Component
public class ProjectEditCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit project";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE PROJECT TO EDIT]");
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        terminalService.printLine(projectEndpoint.getListP(session));
        @Nullable String stringBuf = projectEndpoint.getIdByCountP(session, Integer.parseInt(terminalService.readLine()));
        if (stringBuf == null) throw new Exception("Wrong index.");
        @Nullable final ProjectDTO project = projectEndpoint.findOneP(session, stringBuf);
        terminalService.printLineG("[ENTER NEW NAME]");
        stringBuf = terminalService.readLine();
        if (!stringBuf.isEmpty()&& projectEndpoint.isNameExistP(sessionLocator.getCurrentSession(), stringBuf)) {
            terminalService.printLineR("This name already exist.");
            return;
        }
        if (!stringBuf.isEmpty())
        project.setName(stringBuf);
        terminalService.readWBS(project);
        projectEndpoint.mergeP(sessionLocator.getCurrentSession(), project.getId(), project);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
