package ru.buzanov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.dto.EntityDTO;

import java.io.File;
import java.io.FileOutputStream;
@Component
public class DataJackJsonSaveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "data-jackjson-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data in Jackson Json format.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final EntityDTO dto = new EntityDTO();
        dto.load(sessionLocator);
        @NotNull final File path = new File(FormatConst.SAVE_PATH + "JData.json");
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@NotNull final FileOutputStream outputStream = new FileOutputStream(path)) {
            mapper.writerWithDefaultPrettyPrinter().writeValue(outputStream, dto);
            terminalService.printLineG("[DATA SAVED]");
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
