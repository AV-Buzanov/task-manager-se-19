package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.ProjectDTO;
@Component
public class ProjectCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        terminalService.printLineG("[PROJECT CREATE]");
        terminalService.printG("[ENTER NAME] ");
        @NotNull final String name = terminalService.readLine();
        if (projectEndpoint.isNameExistP(sessionLocator.getCurrentSession(), name)) {
            terminalService.printLineR("This name already exist.");
            return;
        }
        project.setName(name);
        terminalService.printLineG("[FILL DATA(Y/N)?]");
        if (terminalService.readLine().equals("Y"))
            terminalService.readWBS(project);
        projectEndpoint.loadP(sessionLocator.getCurrentSession(), project);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
