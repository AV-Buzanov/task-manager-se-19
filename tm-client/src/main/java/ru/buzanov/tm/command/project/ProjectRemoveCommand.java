package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.SessionDTO;
@Component
public class ProjectRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected project with connected tasks.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE PROJECT TO REMOVE]");
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        terminalService.printLine(projectEndpoint.getListP(session));
        @Nullable String idBuf = projectEndpoint.getIdByCountP(session, Integer.parseInt(terminalService.readLine()));
        projectEndpoint.removeP(session, idBuf);
//        taskEndpoint.removeByProjectIdT(session, idBuf);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
