package ru.buzanov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
@Component
public class AboutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Information about application";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[ABOUT]");
        terminalService.printLineG("TaskDTO-Manager ver.SE-09, ", Manifests.read("buildNumber"));
        terminalService.printLineG("Developer: ", Manifests.read("developer"));
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
