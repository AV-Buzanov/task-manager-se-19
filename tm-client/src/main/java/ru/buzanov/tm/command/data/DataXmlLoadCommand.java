package ru.buzanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.dto.EntityDTO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
@Component
public class DataXmlLoadCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "data-xml-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data in xml format";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(EntityDTO.class);
        @NotNull final Unmarshaller marshaller = jaxbContext.createUnmarshaller();
        @NotNull final File path = new File(FormatConst.SAVE_PATH + "Data.xml");
        try (@NotNull final FileInputStream inputStream = new FileInputStream(path)) {
            @NotNull final EntityDTO dto = (EntityDTO) marshaller.unmarshal(inputStream);
            projectEndpoint.loadListP(sessionLocator.getCurrentSession(), dto.getProjects());
            taskEndpoint.loadListT(sessionLocator.getCurrentSession(), dto.getTasks());
            terminalService.printLineG("[DATA LOADED]");
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
