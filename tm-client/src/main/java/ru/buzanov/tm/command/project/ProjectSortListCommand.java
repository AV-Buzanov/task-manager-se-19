package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Exception_Exception;
import ru.buzanov.tm.endpoint.Field;
import ru.buzanov.tm.endpoint.ProjectDTO;
import ru.buzanov.tm.endpoint.SessionDTO;

import java.util.*;
@Component
public class ProjectSortListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-list-sorted";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects sorted";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[SORTED PROJECT LIST]");
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        @NotNull List<ProjectDTO> list = new ArrayList<>();
        terminalService.printLineG("[SORT BY]");
        terminalService.printLine("1 : By creation");
        terminalService.printLine("2 : By name");
        terminalService.printLine("3 : By start date");
        terminalService.printLine("4 : By end date");
        terminalService.printLine("5 : By tasks");
        terminalService.printLine("6 : By status");
        @NotNull final String comp = terminalService.readLine();
        terminalService.printLineG("[DIRECTION]");
        terminalService.printLine("1 : Rising");
        terminalService.printLine("2 : Falling");
        boolean dir = true;
        if ("2".equals(terminalService.readLine()))
            dir = false;
        switch (comp) {
            case ("1"):
                list = projectEndpoint.findAllP(session);
                if (!dir)
                    Collections.reverse(list);
                break;
            case ("2"):
                list = projectEndpoint.findAllOrderedP(session, dir, Field.NAME);
                break;
            case ("3"):
                list = projectEndpoint.findAllOrderedP(session, dir, Field.START_DATE);
                break;
            case ("4"):
                list = projectEndpoint.findAllOrderedP(session, dir, Field.FINISH_DATE);
                break;
            case ("5"):
                list = projectEndpoint.findAllP(session);
                if (dir)
                    list.sort(new Comparator<ProjectDTO>() {
                        @Override
                        public int compare(ProjectDTO o1, ProjectDTO o2) {
                            int o1size = 0;
                            try {
                                o1size = Objects.requireNonNull(taskEndpoint.findByProjectIdT(sessionLocator.getCurrentSession(), o1.getId())).size();
                            } catch (Exception_Exception e) {
                                e.printStackTrace();
                            }
                            int o2size = 0;
                            try {
                                o2size = Objects.requireNonNull(taskEndpoint.findByProjectIdT(sessionLocator.getCurrentSession(), o2.getId())).size();
                            } catch (Exception_Exception e) {
                                e.printStackTrace();
                            }
                            return Integer.compare(o1size, o2size);
                        }
                    });
                else
                    list.sort(new Comparator<ProjectDTO>() {
                        @Override
                        public int compare(ProjectDTO o1, ProjectDTO o2) {
                            int o1size = 0;
                            try {
                                o1size = Objects.requireNonNull(taskEndpoint.findByProjectIdT(sessionLocator.getCurrentSession(), o1.getId())).size();
                            } catch (Exception_Exception e) {
                                e.printStackTrace();
                            }
                            int o2size = 0;
                            try {
                                o2size = Objects.requireNonNull(taskEndpoint.findByProjectIdT(sessionLocator.getCurrentSession(), o2.getId())).size();
                            } catch (Exception_Exception e) {
                                e.printStackTrace();
                            }
                            return Integer.compare(o1size, o2size) * (-1);
                        }
                    });
                break;
            case ("6"):
                list = projectEndpoint.findAllP(session);
                list.sort(terminalService.getStatusComparator(dir));
                break;
            default:
                break;
        }
        for (@NotNull final ProjectDTO project : list) {
            terminalService.printWBS(project);
            terminalService.printLine();
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
