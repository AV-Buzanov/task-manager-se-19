package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.endpoint.ProjectDTO;
import ru.buzanov.tm.endpoint.SessionDTO;
import ru.buzanov.tm.endpoint.TaskDTO;
@Component
public class ProjectViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-view";
    }

    @NotNull
    @Override
    public String description() {
        return "View project information";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE PROJECT TO VIEW]");
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        terminalService.printLine(projectEndpoint.getListP(session));
        @Nullable String idBuf = projectEndpoint.getIdByCountP(session, Integer.parseInt(terminalService.readLine()));
        @NotNull final ProjectDTO project = projectEndpoint.findOneP(session, idBuf);
        terminalService.printWBSLine(project);
        terminalService.printLineG("[TASKS(", String.valueOf(taskEndpoint.findByProjectIdT(session, idBuf).size()), ")]");
        if (taskEndpoint.findByProjectIdT(session, idBuf).isEmpty()) {
            terminalService.printLine(FormatConst.EMPTY_FIELD);
            return;
        }
        for (@NotNull final TaskDTO task : taskEndpoint.findByProjectIdT(session, idBuf)) {
            terminalService.printLine(task.getName(), " : ", task.getStatus().value());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
