package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.SessionDTO;
@Component
public class UserLogOutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "log-out";
    }

    @NotNull
    @Override
    public String description() {
        return "UserDTO log-out";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO session = sessionLocator.getCurrentSession();
        sessionLocator.setCurrentUser(null);
        sessionLocator.setCurrentSession(null);
        sessionEndpoint.killSession(session);
        terminalService.printLineG("[YOU ARE UNAUTHORIZED NOW]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
