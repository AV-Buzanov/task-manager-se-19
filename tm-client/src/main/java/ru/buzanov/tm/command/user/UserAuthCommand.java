package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.SessionDTO;
@Component
public class UserAuthCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "auth";
    }

    @NotNull
    @Override
    public String description() {
        return "UserDTO authentication";
    }

    @Override
    public void execute() throws Exception {
        if (sessionLocator.getCurrentSession() != null) {
            terminalService.printLineG("[LOG OUT BEFORE AUTHORISATION]");
            return;
        }
        terminalService.printLineG("[AUTHORISATION]");
        terminalService.printLineG("[ENTER LOGIN]");
        @NotNull final String login = terminalService.readLine();
        if (!userEndpoint.isLoginExist(login)) {
            terminalService.printLineR("This login doesn't exist.");
            return;
        }
        terminalService.printLineG("[ENTER PASS]");
        @NotNull final String pass = terminalService.readLine();
        @NotNull final SessionDTO session = sessionEndpoint.getSession(login, pass);
        sessionLocator.setCurrentSession(session);
        sessionLocator.setCurrentUser(userEndpoint.findOne(session));
        terminalService.printLineG("[HELLO, " + sessionLocator.getCurrentUser().getName() + ", NICE TO SEE YOU!]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
