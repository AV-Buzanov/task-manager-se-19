package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.ProjectDTO;
import ru.buzanov.tm.endpoint.SessionDTO;
@Component
public class ProjectListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[PROJECT LIST]");
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        for (ProjectDTO project : projectEndpoint.findAllP(session)) {
            terminalService.printWBS(project);
            terminalService.printLine();
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
