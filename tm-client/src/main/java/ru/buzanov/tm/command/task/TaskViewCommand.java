package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.SessionDTO;
import ru.buzanov.tm.endpoint.TaskDTO;
@Component
public class TaskViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-view";
    }

    @NotNull
    @Override
    public String description() {
        return "View task information";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE TASK TO VIEW]");
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        terminalService.printLine(taskEndpoint.getListT(session));
        @Nullable String idBuf = taskEndpoint.getIdByCountT(session, Integer.parseInt(terminalService.readLine()));
        @Nullable final TaskDTO task = taskEndpoint.findOneT(session, idBuf);
        terminalService.printWBSLine(task);
        terminalService.printG("[PROJECT]");
        terminalService.printLine(projectEndpoint.findOneP(session, task.getProjectId()).getName());
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
