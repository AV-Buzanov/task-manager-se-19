package ru.buzanov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.api.SessionLocator;
import ru.buzanov.tm.api.TerminalService;
import ru.buzanov.tm.endpoint.Exception;
import ru.buzanov.tm.endpoint.*;

@Component
public abstract class AbstractCommand {
    @Autowired
    protected SessionLocator sessionLocator;
    @Autowired
    protected TerminalService terminalService;
    @Autowired
    protected UserEndpoint userEndpoint;
    @Autowired
    protected ProjectEndpoint projectEndpoint;
    @Autowired
    protected TaskEndpoint taskEndpoint;
    @Autowired
    protected SessionEndpoint sessionEndpoint;
    @Autowired
    protected AdminUserEndpoint adminUserEndpoint;

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception, java.lang.Exception;

    public abstract boolean isSecure() throws Exception, java.lang.Exception;

    public boolean isRoleAllow(RoleType role) {
        return true;
    }
}