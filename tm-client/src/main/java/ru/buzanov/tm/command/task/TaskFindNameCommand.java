package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.endpoint.ProjectDTO;
import ru.buzanov.tm.endpoint.SessionDTO;
import ru.buzanov.tm.endpoint.TaskDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@Component
public class TaskFindNameCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-find";
    }

    @NotNull
    @Override
    public String description() {
        return "Find tasks by name, description or project name.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        @NotNull List<TaskDTO> list = new ArrayList<>();
        terminalService.printLineG("[FIND BY]");
        terminalService.printLine("1 : Name");
        terminalService.printLine("2 : Description");
        terminalService.printLine("3 : ProjectDTO name");
        @NotNull final String s = terminalService.readLine();
        switch (s) {
            case ("1"):
                terminalService.printLineG("[ENTER NAME]");
                list = taskEndpoint.findByNameT(session, terminalService.readLine());
                break;
            case ("2"):
                terminalService.printLineG("[ENTER DESCRIPTION]");
                list = taskEndpoint.findByDescriptionT(session, terminalService.readLine());
                break;
            case ("3"):
                terminalService.printLineG("[ENTER PROJECT NAME]");
                @NotNull final String projectname = terminalService.readLine();
                for (ProjectDTO project : Objects.requireNonNull(projectEndpoint.findByNameP(session, projectname)))
                    list.addAll(Objects.requireNonNull(taskEndpoint.findByProjectIdT(session, project.getId())));
                break;
            default:
                break;
        }
        if (list.isEmpty())
            terminalService.printLine(FormatConst.EMPTY_FIELD);
        else {
            for (@NotNull final TaskDTO task : list) {
                terminalService.printWBS(task);
                terminalService.printG("[PROJECT] ");
                terminalService.printLine(projectEndpoint.findOneP(session, task.getProjectId()).getName());
            }
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
