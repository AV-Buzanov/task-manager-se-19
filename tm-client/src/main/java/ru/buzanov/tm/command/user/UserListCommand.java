package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.RoleType;
import ru.buzanov.tm.endpoint.SessionDTO;
import ru.buzanov.tm.endpoint.UserDTO;

import java.lang.annotation.Annotation;

@Component
public class UserListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "user-list";
    }

    @NotNull
    @Override
    public String description() {
        return "View user list (for admin only)";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[USER LIST]");
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        for (@NotNull final UserDTO user : adminUserEndpoint.findAll(session)) {
            terminalService.printG("[LOGIN] ");
            terminalService.print(user.getLogin());
            terminalService.printG(" [NAME] ");
            terminalService.print(user.getName());
            terminalService.printG(" [ROLE] ");
            terminalService.printLine(user.getRoleType().value());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }

    @Override
    public boolean isRoleAllow(RoleType role) {
        if (RoleType.ПОЛЬЗОВАТЕЛЬ.equals(role))
            return false;
        return super.isRoleAllow(role);
    }
}
