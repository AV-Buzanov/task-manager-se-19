package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.SessionDTO;

import java.util.Objects;
@Component
public class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE TASK TO REMOVE]");
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        terminalService.printLine(taskEndpoint.getListT(session));
        @Nullable String idBuf = taskEndpoint.getIdByCountT(session, Integer.parseInt(terminalService.readLine()));
        taskEndpoint.removeT(session, Objects.requireNonNull(idBuf));
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
