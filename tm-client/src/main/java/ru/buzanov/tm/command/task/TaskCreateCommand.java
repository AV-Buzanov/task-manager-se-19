package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.SessionDTO;
import ru.buzanov.tm.endpoint.TaskDTO;
@Component
public class TaskCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        terminalService.printLineG("[TASK CREATE]");
        terminalService.printG("[ENTER NAME]");
        @NotNull String stringBuf = terminalService.readLine();
        if (taskEndpoint.isNameExistT(sessionLocator.getCurrentSession(), stringBuf)) {
            terminalService.printLineR("This name already exist.");
            return;
        }
        task.setName(stringBuf);
        terminalService.printLine("[FILL DATA(Y/N)?]");
        if (terminalService.readLine().equals("Y"))
            terminalService.readWBS(task);
        if (!projectEndpoint.findAllP(session).isEmpty()) {
            terminalService.printLineG("[CHOOSE PROJECT]");
            terminalService.printLine(projectEndpoint.getListP(session));
            stringBuf = terminalService.readLine();
            if (!stringBuf.isEmpty()) {
                task.setProjectId(projectEndpoint.getIdByCountP(session, Integer.parseInt(stringBuf)));
            }
        }
        taskEndpoint.loadT(session, task);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
