package ru.buzanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
@Component
public class ExitCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() throws Exception {
        StringBuilder s = new StringBuilder("[GOODBYE! TILL WE MEET AGAIN!]");
        if (sessionLocator.getCurrentUser() != null) {
            s.insert(8, ", ").insert(10, sessionLocator.getCurrentUser().getName());
            sessionEndpoint.killSession(sessionLocator.getCurrentSession());
        }
        terminalService.printLineG(s.toString());
        System.exit(0);
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
