package ru.buzanov.tm.command.data;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.dto.EntityDTO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
@Component
public class DataJsonSaveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "data-json-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Data save json";
    }

    @Override
    public void execute() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jc = JAXBContext.newInstance(EntityDTO.class);
        @NotNull final Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final EntityDTO dto = new EntityDTO();
        dto.load(sessionLocator);
        @NotNull final File path = new File(FormatConst.SAVE_PATH + "Data.json");
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@NotNull final FileOutputStream outputStream = new FileOutputStream(path)) {
            marshaller.marshal(dto, outputStream);
            terminalService.printLineG("[DATA SAVED]");
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
