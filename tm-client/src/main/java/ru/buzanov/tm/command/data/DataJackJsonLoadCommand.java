package ru.buzanov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.dto.EntityDTO;

import java.io.File;
import java.io.FileInputStream;
@Component
public class DataJackJsonLoadCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "data-jackjson-load";
    }

    @Override
    public @NotNull String description() {
        return "Load data in Jackson Json format.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final File path = new File(FormatConst.SAVE_PATH + "JData.json");
        try (@NotNull final FileInputStream inputStream = new FileInputStream(path)) {
            @NotNull final EntityDTO dto = mapper.readValue(inputStream, EntityDTO.class);
            projectEndpoint.loadListP(sessionLocator.getCurrentSession(), dto.getProjects());
            taskEndpoint.loadListT(sessionLocator.getCurrentSession(), dto.getTasks());
            terminalService.printLineG("[DATA LOADED]");
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
