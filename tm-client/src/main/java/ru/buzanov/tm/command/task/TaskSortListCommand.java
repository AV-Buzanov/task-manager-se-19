package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Field;
import ru.buzanov.tm.endpoint.SessionDTO;
import ru.buzanov.tm.endpoint.TaskDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Component
public class TaskSortListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-list-sorted";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks sorted";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[SORTED TASK LIST]");
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        @NotNull List<TaskDTO> list = new ArrayList<>();
        terminalService.printLineG("[SORT BY]");
        terminalService.printLine("1 : By creation");
        terminalService.printLine("2 : By name");
        terminalService.printLine("3 : By start date");
        terminalService.printLine("4 : By end date");
        terminalService.printLine("5 : By status");
        @NotNull final String comp = terminalService.readLine();
        terminalService.printLineG("[DIRECTION]");
        terminalService.printLine("1 : Rising");
        terminalService.printLine("2 : Falling");
        boolean dir = true;
        if ("2".equals(terminalService.readLine()))
            dir = false;
        switch (comp) {
            case ("1"):
                list = taskEndpoint.findAllT(session);
                if (!dir)
                    Collections.reverse(list);
                break;
            case ("2"):
                list = taskEndpoint.findAllOrderedT(session, dir, Field.NAME);
                break;
            case ("3"):
                list = taskEndpoint.findAllOrderedT(session, dir, Field.START_DATE);
                break;
            case ("4"):
                list = taskEndpoint.findAllOrderedT(session, dir, Field.FINISH_DATE);
                break;
            case ("5"):
                list.sort(terminalService.getStatusComparator(dir));
                break;
            default:
                break;
        }
        for (@NotNull final TaskDTO task : list) {
            terminalService.printWBS(task);
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
