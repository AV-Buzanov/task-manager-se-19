package ru.buzanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.dto.EntityDTO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
@Component
public class DataXmlSaveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "data-xml-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data in xml format.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final EntityDTO dto = new EntityDTO();
        dto.load(sessionLocator);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(EntityDTO.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final File path = new File(FormatConst.SAVE_PATH + "Data.xml");
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@NotNull final FileOutputStream outputStream = new FileOutputStream(path)) {
            marshaller.marshal(dto, outputStream);
            terminalService.printLineG("[DATA SAVED]");
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
