package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
@Component
public class UserRegisterCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "register";
    }

    @NotNull
    @Override
    public String description() {
        return "UserDTO registration";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[REGISTRATION]");
        terminalService.printLineG("[ENTER LOGIN]");
        @NotNull final String login = terminalService.readLine();
        if (login.isEmpty()) {
            terminalService.printLine("Login can't be empty");
            return;
        }
        if (userEndpoint.isLoginExist(login)) {
            terminalService.printLine("This login already exist");
            return;
        }
        terminalService.printLineG("[ENTER PASS]");
        @NotNull final String pass = terminalService.readLine();
        if (pass.isEmpty() || pass.length() < 6) {
            terminalService.printLine("Pass can't be empty and less then 6 symbols");
            return;
        }
        terminalService.printLineG("[REPEAT PASS]");
        if (!pass.equals(terminalService.readLine())) {
            terminalService.printLine("Invalid pass");
            return;
        }
        userEndpoint.registryUser(login, pass);
        terminalService.printLineG("[Registration success, now type auth to log in]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
