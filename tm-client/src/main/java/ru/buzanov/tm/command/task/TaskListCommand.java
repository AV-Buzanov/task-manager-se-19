package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.ProjectDTO;
import ru.buzanov.tm.endpoint.SessionDTO;
import ru.buzanov.tm.endpoint.TaskDTO;

import java.util.Objects;
@Component
public class TaskListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[TASK LIST]");
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        for (@NotNull final TaskDTO task : Objects.requireNonNull(taskEndpoint.findAllT(session))) {
            terminalService.printWBS(task);
            terminalService.printG(" [PROJECT] ");
            ProjectDTO project = projectEndpoint.findOneP(session, task.getProjectId());
            if (project != null)
                terminalService.printLine(project.getName());
            terminalService.printLine();
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
