package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.SessionDTO;
import ru.buzanov.tm.endpoint.TaskDTO;

import java.util.Objects;
@Component
public class TaskEditCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        terminalService.printLineG("[CHOOSE TASK TO EDIT]");
        terminalService.printLine(taskEndpoint.getListT(session));
        @Nullable String stringBuf = taskEndpoint.getIdByCountT(session, Integer.parseInt(terminalService.readLine()));
        @NotNull final TaskDTO task = Objects.requireNonNull(taskEndpoint.findOneT(session, stringBuf));
        terminalService.printLineG("[NAME]");
        terminalService.printLine(task.getName());
        terminalService.printLineG("[ENTER NEW NAME]");
        stringBuf = terminalService.readLine();
        if (!stringBuf.isEmpty()&& taskEndpoint.isNameExistT(sessionLocator.getCurrentSession(), stringBuf)) {
            terminalService.printLineR("This name already exist.");
            return;
        }
        if (!stringBuf.isEmpty())
        task.setName(stringBuf);
        terminalService.readWBS(task);
        terminalService.printLineG("[PROJECT]");
        terminalService.printLine(projectEndpoint.findOneP(session, task.getProjectId()).getName());
        terminalService.printLineG("[CHOOSE NEW PROJECT, WRITE 0 TO REMOVE]");
        terminalService.printLine(projectEndpoint.getListP(session));
        stringBuf = terminalService.readLine();
        if (!stringBuf.isEmpty()) {
            if ("0".equals(stringBuf))
                stringBuf = null;
            else stringBuf = projectEndpoint.getIdByCountP(session, Integer.parseInt(stringBuf));
            task.setProjectId(stringBuf);
        }
        taskEndpoint.mergeT(session, task.getId(), task);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
