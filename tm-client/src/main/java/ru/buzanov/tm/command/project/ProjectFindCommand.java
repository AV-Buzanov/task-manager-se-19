package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.endpoint.ProjectDTO;
import ru.buzanov.tm.endpoint.SessionDTO;

import java.util.ArrayList;
import java.util.List;
@Component
public class ProjectFindCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-find";
    }

    @NotNull
    @Override
    public String description() {
        return "Find projects by name or description.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final SessionDTO session = sessionLocator.getCurrentSession();
        @NotNull List<ProjectDTO> list = new ArrayList<>();
        terminalService.printLineG("[FIND BY]");
        terminalService.printLine("1 : Name");
        terminalService.printLine("2 : Description");
        @NotNull final String s = terminalService.readLine();
        switch (s) {
            case ("1"):
                terminalService.printLineG("[ENTER NAME]");
                list = projectEndpoint.findByNameP(session, terminalService.readLine());
                break;
            case ("2"):
                terminalService.printLineG("[ENTER DESCRIPTION]");
                list = projectEndpoint.findByDescriptionP(session, terminalService.readLine());
                break;
            default:
                break;
        }
        if (list.isEmpty())
            terminalService.printLine(FormatConst.EMPTY_FIELD);
        else {
            for (@NotNull final ProjectDTO project : list) {
                terminalService.printWBS(project);
                terminalService.printLine();
            }
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
