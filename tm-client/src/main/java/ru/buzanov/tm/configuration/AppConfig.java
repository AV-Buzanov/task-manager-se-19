package ru.buzanov.tm.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ru.buzanov.tm.endpoint.*;

import java.lang.Exception;
import java.net.URL;

@Configuration
@ComponentScan(basePackages = "ru.buzanov.tm")
@PropertySource("classpath:connection.properties")
public class AppConfig {

    @Value("${ip}")
    private String ip;

    @Value("${port}")
    private String port;

    @Bean
    public ProjectEndpoint projectEndpoint() {
        try {
            return new ProjectEndpointService(new URL("http://" + ip + ":" + port + "/ProjectEndpoint?wsdl")).getProjectEndpointPort();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Bean
    public TaskEndpoint taskEndpoint() {
        try {
            return new TaskEndpointService(new URL("http://" + ip + ":" + port + "/TaskEndpoint?wsdl")).getTaskEndpointPort();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Bean
    public UserEndpoint userEndpoint() {
        try {
            return new UserEndpointService(new URL("http://" + ip + ":" + port + "/UserEndpoint?wsdl")).getUserEndpointPort();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Bean
    public SessionEndpoint sessionEndpoint() {
        try {
            return new SessionEndpointService(new URL("http://" + ip + ":" + port + "/SessionEndpoint?wsdl")).getSessionEndpointPort();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Bean
    public AdminUserEndpoint adminUserEndpoint() {
        try {
            return new AdminUserEndpointService(new URL("http://" + ip + ":" + port + "/AdminUserEndpoint?wsdl")).getAdminUserEndpointPort();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
