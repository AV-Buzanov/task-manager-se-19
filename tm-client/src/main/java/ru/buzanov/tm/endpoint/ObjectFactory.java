
package ru.buzanov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.buzanov.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.buzanov.ru/", "Exception");
    private final static QName _FindAll_QNAME = new QName("http://endpoint.tm.buzanov.ru/", "findAll");
    private final static QName _FindAllResponse_QNAME = new QName("http://endpoint.tm.buzanov.ru/", "findAllResponse");
    private final static QName _FindByLogin_QNAME = new QName("http://endpoint.tm.buzanov.ru/", "findByLogin");
    private final static QName _FindByLoginResponse_QNAME = new QName("http://endpoint.tm.buzanov.ru/", "findByLoginResponse");
    private final static QName _FindByRole_QNAME = new QName("http://endpoint.tm.buzanov.ru/", "findByRole");
    private final static QName _FindByRoleResponse_QNAME = new QName("http://endpoint.tm.buzanov.ru/", "findByRoleResponse");
    private final static QName _FindOneAdmin_QNAME = new QName("http://endpoint.tm.buzanov.ru/", "findOneAdmin");
    private final static QName _FindOneAdminResponse_QNAME = new QName("http://endpoint.tm.buzanov.ru/", "findOneAdminResponse");
    private final static QName _RemoveAdmin_QNAME = new QName("http://endpoint.tm.buzanov.ru/", "removeAdmin");
    private final static QName _RemoveAdminResponse_QNAME = new QName("http://endpoint.tm.buzanov.ru/", "removeAdminResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.buzanov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link FindAll }
     * 
     */
    public FindAll createFindAll() {
        return new FindAll();
    }

    /**
     * Create an instance of {@link FindAllResponse }
     * 
     */
    public FindAllResponse createFindAllResponse() {
        return new FindAllResponse();
    }

    /**
     * Create an instance of {@link FindByLogin }
     * 
     */
    public FindByLogin createFindByLogin() {
        return new FindByLogin();
    }

    /**
     * Create an instance of {@link FindByLoginResponse }
     * 
     */
    public FindByLoginResponse createFindByLoginResponse() {
        return new FindByLoginResponse();
    }

    /**
     * Create an instance of {@link FindByRole }
     * 
     */
    public FindByRole createFindByRole() {
        return new FindByRole();
    }

    /**
     * Create an instance of {@link FindByRoleResponse }
     * 
     */
    public FindByRoleResponse createFindByRoleResponse() {
        return new FindByRoleResponse();
    }

    /**
     * Create an instance of {@link FindOneAdmin }
     * 
     */
    public FindOneAdmin createFindOneAdmin() {
        return new FindOneAdmin();
    }

    /**
     * Create an instance of {@link FindOneAdminResponse }
     * 
     */
    public FindOneAdminResponse createFindOneAdminResponse() {
        return new FindOneAdminResponse();
    }

    /**
     * Create an instance of {@link RemoveAdmin }
     * 
     */
    public RemoveAdmin createRemoveAdmin() {
        return new RemoveAdmin();
    }

    /**
     * Create an instance of {@link RemoveAdminResponse }
     * 
     */
    public RemoveAdminResponse createRemoveAdminResponse() {
        return new RemoveAdminResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link UserDTO }
     * 
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.buzanov.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAll }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.buzanov.ru/", name = "findAll")
    public JAXBElement<FindAll> createFindAll(FindAll value) {
        return new JAXBElement<FindAll>(_FindAll_QNAME, FindAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.buzanov.ru/", name = "findAllResponse")
    public JAXBElement<FindAllResponse> createFindAllResponse(FindAllResponse value) {
        return new JAXBElement<FindAllResponse>(_FindAllResponse_QNAME, FindAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.buzanov.ru/", name = "findByLogin")
    public JAXBElement<FindByLogin> createFindByLogin(FindByLogin value) {
        return new JAXBElement<FindByLogin>(_FindByLogin_QNAME, FindByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.buzanov.ru/", name = "findByLoginResponse")
    public JAXBElement<FindByLoginResponse> createFindByLoginResponse(FindByLoginResponse value) {
        return new JAXBElement<FindByLoginResponse>(_FindByLoginResponse_QNAME, FindByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.buzanov.ru/", name = "findByRole")
    public JAXBElement<FindByRole> createFindByRole(FindByRole value) {
        return new JAXBElement<FindByRole>(_FindByRole_QNAME, FindByRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.buzanov.ru/", name = "findByRoleResponse")
    public JAXBElement<FindByRoleResponse> createFindByRoleResponse(FindByRoleResponse value) {
        return new JAXBElement<FindByRoleResponse>(_FindByRoleResponse_QNAME, FindByRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneAdmin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.buzanov.ru/", name = "findOneAdmin")
    public JAXBElement<FindOneAdmin> createFindOneAdmin(FindOneAdmin value) {
        return new JAXBElement<FindOneAdmin>(_FindOneAdmin_QNAME, FindOneAdmin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneAdminResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.buzanov.ru/", name = "findOneAdminResponse")
    public JAXBElement<FindOneAdminResponse> createFindOneAdminResponse(FindOneAdminResponse value) {
        return new JAXBElement<FindOneAdminResponse>(_FindOneAdminResponse_QNAME, FindOneAdminResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAdmin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.buzanov.ru/", name = "removeAdmin")
    public JAXBElement<RemoveAdmin> createRemoveAdmin(RemoveAdmin value) {
        return new JAXBElement<RemoveAdmin>(_RemoveAdmin_QNAME, RemoveAdmin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAdminResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.buzanov.ru/", name = "removeAdminResponse")
    public JAXBElement<RemoveAdminResponse> createRemoveAdminResponse(RemoveAdminResponse value) {
        return new JAXBElement<RemoveAdminResponse>(_RemoveAdminResponse_QNAME, RemoveAdminResponse.class, null, value);
    }

}
