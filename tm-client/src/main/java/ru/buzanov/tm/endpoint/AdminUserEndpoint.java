package ru.buzanov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-10-18T14:24:51.746+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.buzanov.ru/", name = "AdminUserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminUserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findByRoleRequest", output = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findByRoleResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findByRole/Fault/Exception")})
    @RequestWrapper(localName = "findByRole", targetNamespace = "http://endpoint.tm.buzanov.ru/", className = "ru.buzanov.tm.endpoint.FindByRole")
    @ResponseWrapper(localName = "findByRoleResponse", targetNamespace = "http://endpoint.tm.buzanov.ru/", className = "ru.buzanov.tm.endpoint.FindByRoleResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<UserDTO> findByRole(
        @WebParam(name = "arg0", targetNamespace = "")
                SessionDTO arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        ru.buzanov.tm.endpoint.RoleType arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/removeAdminRequest", output = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/removeAdminResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/removeAdmin/Fault/Exception")})
    @RequestWrapper(localName = "removeAdmin", targetNamespace = "http://endpoint.tm.buzanov.ru/", className = "ru.buzanov.tm.endpoint.RemoveAdmin")
    @ResponseWrapper(localName = "removeAdminResponse", targetNamespace = "http://endpoint.tm.buzanov.ru/", className = "ru.buzanov.tm.endpoint.RemoveAdminResponse")
    @WebResult(name = "return", targetNamespace = "")
    public UserDTO removeAdmin(
        @WebParam(name = "arg0", targetNamespace = "")
                SessionDTO arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findAllRequest", output = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findAllResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findAll/Fault/Exception")})
    @RequestWrapper(localName = "findAll", targetNamespace = "http://endpoint.tm.buzanov.ru/", className = "ru.buzanov.tm.endpoint.FindAll")
    @ResponseWrapper(localName = "findAllResponse", targetNamespace = "http://endpoint.tm.buzanov.ru/", className = "ru.buzanov.tm.endpoint.FindAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<UserDTO> findAll(
        @WebParam(name = "arg0", targetNamespace = "")
                SessionDTO arg0
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findByLoginRequest", output = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findByLoginResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findByLogin/Fault/Exception")})
    @RequestWrapper(localName = "findByLogin", targetNamespace = "http://endpoint.tm.buzanov.ru/", className = "ru.buzanov.tm.endpoint.FindByLogin")
    @ResponseWrapper(localName = "findByLoginResponse", targetNamespace = "http://endpoint.tm.buzanov.ru/", className = "ru.buzanov.tm.endpoint.FindByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public UserDTO findByLogin(
        @WebParam(name = "arg0", targetNamespace = "")
                SessionDTO arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findOneAdminRequest", output = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findOneAdminResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.buzanov.ru/AdminUserEndpoint/findOneAdmin/Fault/Exception")})
    @RequestWrapper(localName = "findOneAdmin", targetNamespace = "http://endpoint.tm.buzanov.ru/", className = "ru.buzanov.tm.endpoint.FindOneAdmin")
    @ResponseWrapper(localName = "findOneAdminResponse", targetNamespace = "http://endpoint.tm.buzanov.ru/", className = "ru.buzanov.tm.endpoint.FindOneAdminResponse")
    @WebResult(name = "return", targetNamespace = "")
    public UserDTO findOneAdmin(
        @WebParam(name = "arg0", targetNamespace = "")
                SessionDTO arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    ) throws Exception_Exception;
}
