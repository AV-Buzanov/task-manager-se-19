package ru.buzanov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.buzanov.tm.bootstrap.Bootstrap;
import ru.buzanov.tm.configuration.AppConfig;

/**
 * TaskDTO Manager
 */

public final class App {
    public static void main(String[] args) {
        @NotNull final ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        @NotNull final Bootstrap bootstrap = applicationContext.getBean("bootstrap", Bootstrap.class);
        bootstrap.start();
    }
}