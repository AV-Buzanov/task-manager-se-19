package ru.buzanov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.api.SessionLocator;
import ru.buzanov.tm.endpoint.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Component
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<ProjectDTO> projects;
    private List<TaskDTO> tasks;
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private TaskEndpoint taskEndpoint;

    public void load(@Nullable final SessionLocator serviceLocator) throws Exception_Exception {
        if (serviceLocator == null) return;
        this.projects = new ArrayList<>(projectEndpoint.findAllP(serviceLocator.getCurrentSession()));
        this.tasks = new ArrayList<>(taskEndpoint.findAllT(serviceLocator.getCurrentSession()));
    }

    public void unLoad(@Nullable final SessionLocator serviceLocator) throws Exception_Exception {
        if (serviceLocator == null) return;
        projectEndpoint.loadListP(serviceLocator.getCurrentSession(), projects);
        taskEndpoint.loadListT(serviceLocator.getCurrentSession(), tasks);
    }
}
