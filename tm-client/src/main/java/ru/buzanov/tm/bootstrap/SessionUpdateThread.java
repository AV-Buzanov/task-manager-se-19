package ru.buzanov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.api.SessionLocator;
import ru.buzanov.tm.endpoint.Exception_Exception;
import ru.buzanov.tm.endpoint.SessionDTO;
import ru.buzanov.tm.endpoint.SessionEndpoint;

@Component
public class SessionUpdateThread extends Thread {
    @NotNull
    @Autowired
    private SessionLocator sessionLocator;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public void run() {
        while (true) {
            try {
                sleep(100000);
                if (sessionLocator.getCurrentSession() != null) {
                    @NotNull final SessionDTO session = sessionEndpoint.updateSession(sessionLocator.getCurrentSession());
                    sessionLocator.setCurrentSession(session);
                }
            } catch (Exception_Exception | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
