package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.api.SessionLocator;
import ru.buzanov.tm.api.TerminalService;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.SessionDTO;
import ru.buzanov.tm.endpoint.UserDTO;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Getter
@Setter
@Component
@NoArgsConstructor
public final class Bootstrap implements SessionLocator {
    @NotNull
    @Autowired
    private TerminalService terminalService;
    @Autowired
    private List<AbstractCommand> commands;
    @NotNull
    private final Map<String, AbstractCommand> commandsMap = new TreeMap<>();
    @Autowired
    private Thread sessionUpdateThread;
    @Nullable
    private SessionDTO currentSession;
    @Nullable
    private UserDTO currentUser;

    public void start() {
        for (AbstractCommand command : commands)
            commandsMap.put(command.command(), command);

        sessionUpdateThread.setDaemon(true);
        sessionUpdateThread.start();

        @NotNull String command = "";

        terminalService.printLineG("***WELCOME TO TASK MANAGER***");
        terminalService.printLine("Type help to see command list.");
        while (!commandsMap.get("exit").command().equals(command)) {
            try {
                command = terminalService.readLine();
                if (!command.isEmpty()) {

                    if (getCurrentSession() == null && commandsMap.get(command).isSecure())
                        terminalService.printLineR("Authorise, please");
                    else {
                        if (getCurrentUser() != null && !commandsMap.get(command).isRoleAllow(getCurrentUser().getRoleType()))
                            terminalService.printLineR("This command is not allow for you");
                        else
                            commandsMap.get(command).execute();
                    }
                }

            } catch (Exception | ru.buzanov.tm.endpoint.Exception e) {
                terminalService.printLineR(e.getMessage(), "  ", e.toString());
            }
        }
    }

    @NotNull
    public Collection<AbstractCommand> getCommandsMap() {
        return commandsMap.values();
    }
}