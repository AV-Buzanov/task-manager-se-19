package ru.buzanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.*;

import java.util.Collection;

@Component
public interface SessionLocator {

    @NotNull Collection<AbstractCommand> getCommandsMap();

    @Nullable SessionDTO getCurrentSession();

    @Nullable UserDTO getCurrentUser();

    void setCurrentSession(@Nullable final SessionDTO session);

    void setCurrentUser(@Nullable final UserDTO user);
}
