package ru.buzanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.endpoint.AbstractWBS;

import java.io.IOException;
import java.util.Comparator;

@Component
public interface TerminalService {

    @NotNull String readLine() throws IOException;

    void printWBS(@Nullable final AbstractWBS entity);

    void printWBSLine(@Nullable final AbstractWBS entity);

    void readWBS(@Nullable final AbstractWBS entity) throws Exception;

    void print(@Nullable final String... strings);

    void printG(@Nullable final String... strings);

    void printLine();

    void printLine(@Nullable final String... strings);

    void printLineG(@Nullable final String... strings);

    void printLineR(@Nullable final String... strings);

    @NotNull Comparator<AbstractWBS> getStatusComparator(final boolean direction);
}
