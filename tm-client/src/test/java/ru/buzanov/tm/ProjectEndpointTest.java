package ru.buzanov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.buzanov.tm.endpoint.*;
import ru.buzanov.tm.util.DateUtil;

import java.lang.Exception;
import java.net.URL;
import java.util.Date;
import java.util.List;

@Category(IntagratedTests.class)
public class ProjectEndpointTest extends Assert {
    @Nullable
    private ProjectEndpoint projectEndpoint;
    @Nullable
    private SessionDTO session;
    @Nullable
    private SessionDTO testSession;
    @Nullable
    private UserEndpoint userEndpoint;
    @Nullable
    private SessionEndpoint sessionEndpoint;
    @Nullable
    private TaskEndpoint taskEndpoint;

    @Before
    public void setUp() throws Exception {
        this.projectEndpoint = new ProjectEndpointService(new URL("http://localhost:8080/ProjectEndpoint?wsdl")).getProjectEndpointPort();
        this.userEndpoint = new UserEndpointService(new URL("http://localhost:8080/UserEndpoint?wsdl")).getUserEndpointPort();
        this.sessionEndpoint = new SessionEndpointService(new URL("http://localhost:8080/SessionEndpoint?wsdl")).getSessionEndpointPort();
        this.taskEndpoint = new TaskEndpointService(new URL("http://localhost:8080/TaskEndpoint?wsdl")).getTaskEndpointPort();
        userEndpoint.registryUser("testUser", "123456");
        userEndpoint.registryUser("testUser2","123456");
        this.session = sessionEndpoint.getSession("testUser", "123456");
        this.testSession = sessionEndpoint.getSession("testUser2", "123456");
        assertNotNull(session);
        assertNotNull(projectEndpoint);
        assertNotNull(userEndpoint);
        assertNotNull(sessionEndpoint);
        assertNotNull(taskEndpoint);
    }

    @After
    public void tearDown() throws Exception {
        userEndpoint.remove(session);
        userEndpoint.remove(testSession);
    }

    @Test
    public void testProjectCreate() throws Exception_Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectEndpoint.loadP(session, project);
        @Nullable final ProjectDTO testProject = projectEndpoint.findOneP(session, project.getId());
        assertNotNull(testProject);
        assertEquals("not equals id", project.getId(), testProject.getId());
        assertEquals("not equals userId", session.getUserId(), testProject.getUserId());
        assertEquals("not equals Name", project.getName(), testProject.getName());
        assertEquals("not equals createDate", DateUtil.toDate(project.getCreateDate()).toString(), DateUtil.toDate(testProject.getCreateDate()).toString());
        assertEquals("not equals status", Status.ЗАПЛАНИРОВАННО, testProject.getStatus());
    }

    @Test(expected = Exception_Exception.class)
    public void testProjectCreateExistName() throws Exception_Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectEndpoint.loadP(session, project);
        @NotNull final ProjectDTO testProject = new ProjectDTO();
        testProject.setName("Name");
        projectEndpoint.loadP(session, testProject);
    }

    @Test
    public void testProjectMerge() throws Exception_Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        project.setDescription("description");
        project.setStartDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        project.setFinishDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        projectEndpoint.loadP(session, project);
        project.setName("newName");
        project.setDescription("newDescription");
        project.setStartDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        project.setFinishDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        project.setStatus(Status.В_ПРОЦЕССЕ);
        projectEndpoint.mergeP(session, project.getId(), project);
        @Nullable final ProjectDTO testProject = projectEndpoint.findOneP(session, project.getId());
        checkEquals(testProject, project);
    }

    @Test
    public void testProjectFind() throws Exception_Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        project.setDescription("description");
        project.setStartDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        project.setFinishDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        project.setStatus(Status.ЗАПЛАНИРОВАННО);
        projectEndpoint.loadP(session, project);
        @Nullable final ProjectDTO testProject = projectEndpoint.findOneP(session, project.getId());
        checkEquals(testProject, project);
    }

    @Test
    public void testProjectFindAllSize() throws Exception_Exception {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectEndpoint.loadP(session, project);
        project = new ProjectDTO();
        project.setName("Name2");
        projectEndpoint.loadP(session, project);
        project = new ProjectDTO();
        project.setName("Name3");
        projectEndpoint.loadP(session, project);
        @Nullable final List<ProjectDTO> list = projectEndpoint.findAllP(session);
        assertNotNull(list);
        assertEquals("size error", 3, list.size());
    }

    @Test
    public void testProjectRemove() throws Exception_Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectEndpoint.loadP(session, project);
        projectEndpoint.removeP(session, project.getId());
        @Nullable final ProjectDTO testProject = projectEndpoint.findOneP(session, project.getId());
        assertNull(testProject);
    }

    @Test
    public void testProjectRemoveWithTask() throws Exception_Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectEndpoint.loadP(session, project);
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("TaskName");
        task.setProjectId(project.getId());
        projectEndpoint.removeP(session, project.getId());
        @Nullable final ProjectDTO testProject = projectEndpoint.findOneP(session, project.getId());
        @Nullable final TaskDTO testTask = taskEndpoint.findOneT(session, task.getId());
        assertNull(testProject);
        assertNull(testTask);
    }

    @Test(expected = Exception_Exception.class)
    public void testProjectCreateWithDeadSession() throws Exception_Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        @Nullable final SessionDTO testSession = sessionEndpoint.getSession("testUser", "123456");
        sessionEndpoint.killSession(testSession);
        projectEndpoint.loadP(testSession, project);
    }

    @Test(expected = Exception_Exception.class)
    public void testProjectFindWithDeadSession() throws Exception_Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        @Nullable final SessionDTO testSession = sessionEndpoint.getSession("testUser", "123456");
        projectEndpoint.loadP(testSession, project);
        sessionEndpoint.killSession(testSession);
        @Nullable final ProjectDTO testProject = projectEndpoint.findOneP(testSession, project.getId());
    }

    @Test(expected = Exception_Exception.class)
    public void testProjectRemoveWithDeadSession() throws Exception_Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        @Nullable final SessionDTO testSession = sessionEndpoint.getSession("testUser", "123456");
        projectEndpoint.loadP(testSession, project);
        sessionEndpoint.killSession(testSession);
        projectEndpoint.removeP(testSession, project.getId());
    }

    @Test
    public void testProjectFindWithWrongUser() throws Exception_Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectEndpoint.loadP(session, project);
        @Nullable final ProjectDTO testProject = projectEndpoint.findOneP(testSession, project.getId());
        assertNull(testProject);
    }

    @Test
    public void testProjectRemoveWithWrongUser() throws Exception_Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectEndpoint.loadP(session, project);
        projectEndpoint.removeP(testSession, project.getId());
        assertNotNull(projectEndpoint.findOneP(session, project.getId()));
    }

    private void checkEquals(@Nullable final ProjectDTO testProject, @Nullable final ProjectDTO project){
        assertNotNull(testProject);
        assertNotNull(project);
        assertEquals("not equals id", project.getId(), testProject.getId());
        assertEquals("not equals userId", session.getUserId(), testProject.getUserId());
        assertEquals("not equals Name", project.getName(), testProject.getName());
        assertEquals("not equals desc", project.getDescription(), testProject.getDescription());
        assertEquals("not equals createDate", DateUtil.toDate(project.getCreateDate()).toString(), DateUtil.toDate(testProject.getCreateDate()).toString());
        assertEquals("not equals startDate", DateUtil.toDate(project.getStartDate()).toString(), DateUtil.toDate(testProject.getStartDate()).toString());
        assertEquals("not equals finishDate", DateUtil.toDate(project.getFinishDate()).toString(), DateUtil.toDate(testProject.getFinishDate()).toString());
        assertEquals("not equals status", project.getStatus(), testProject.getStatus());
    }
}