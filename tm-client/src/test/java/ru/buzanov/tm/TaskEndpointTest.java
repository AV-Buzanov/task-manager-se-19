package ru.buzanov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.buzanov.tm.endpoint.*;
import ru.buzanov.tm.util.DateUtil;

import java.lang.Exception;
import java.net.URL;
import java.util.Date;
import java.util.List;

@Category(IntagratedTests.class)
public class TaskEndpointTest extends Assert {
    @Nullable
    private ProjectEndpoint projectEndpoint;
    @Nullable
    private SessionDTO session;
    @Nullable
    private SessionDTO testSession;
    @Nullable
    private UserEndpoint userEndpoint;
    @Nullable
    private SessionEndpoint sessionEndpoint;
    @Nullable
    private TaskEndpoint taskEndpoint;

    @Before
    public void setUp() throws Exception {
        this.projectEndpoint = new ProjectEndpointService(new URL("http://localhost:8080/ProjectEndpoint?wsdl")).getProjectEndpointPort();
        this.userEndpoint = new UserEndpointService(new URL("http://localhost:8080/UserEndpoint?wsdl")).getUserEndpointPort();
        this.sessionEndpoint = new SessionEndpointService(new URL("http://localhost:8080/SessionEndpoint?wsdl")).getSessionEndpointPort();
        this.taskEndpoint = new TaskEndpointService(new URL("http://localhost:8080/TaskEndpoint?wsdl")).getTaskEndpointPort();
        userEndpoint.registryUser("testUser", "123456");
        userEndpoint.registryUser("testUser2","123456");
        this.session = sessionEndpoint.getSession("testUser", "123456");
        this.testSession = sessionEndpoint.getSession("testUser2", "123456");
        assertNotNull(session);
        assertNotNull(projectEndpoint);
        assertNotNull(userEndpoint);
        assertNotNull(sessionEndpoint);
        assertNotNull(taskEndpoint);
    }

    @After
    public void tearDown() throws Exception {
        userEndpoint.remove(session);
        userEndpoint.remove(testSession);
    }

    @Test
    public void testProjectCreate() throws Exception_Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskEndpoint.loadT(session, task);
        @Nullable final TaskDTO testTask = taskEndpoint.findOneT(session, task.getId());
        assertNotNull(testTask);
        assertEquals("not equals id", task.getId(), testTask.getId());
        assertEquals("not equals userId", session.getUserId(), testTask.getUserId());
        assertEquals("not equals Name", task.getName(), testTask.getName());
        assertEquals("not equals createDate", DateUtil.toDate(task.getCreateDate()).toString(), DateUtil.toDate(testTask.getCreateDate()).toString());
        assertEquals("not equals status", Status.ЗАПЛАНИРОВАННО, testTask.getStatus());
    }

    @Test(expected = Exception_Exception.class)
    public void testTaskCreateExistName() throws Exception_Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskEndpoint.loadT(session, task);
        @NotNull final TaskDTO testTask = new TaskDTO();
        testTask.setName("Name");
        taskEndpoint.loadT(session, testTask);
    }

    @Test
    public void testTaskMerge() throws Exception_Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        task.setDescription("description");
        task.setStartDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        task.setFinishDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        taskEndpoint.loadT(session, task);
        task.setName("newName");
        task.setDescription("newDescription");
        task.setStartDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        task.setFinishDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        task.setStatus(Status.В_ПРОЦЕССЕ);
        taskEndpoint.mergeT(session, task.getId(), task);
        @Nullable final TaskDTO testTask = taskEndpoint.findOneT(session, task.getId());
        checkEquals(testTask, task);
    }

    @Test
    public void testTaskFind() throws Exception_Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        task.setDescription("description");
        task.setStartDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        task.setFinishDate(DateUtil.toXMLGregorianCalendar(new Date(System.currentTimeMillis())));
        task.setStatus(Status.ЗАПЛАНИРОВАННО);
        taskEndpoint.loadT(session, task);
        @Nullable final TaskDTO testTask = taskEndpoint.findOneT(session, task.getId());
        checkEquals(testTask, task);
    }

    @Test
    public void testTaskFindAllSize() throws Exception_Exception {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskEndpoint.loadT(session, task);
        task = new TaskDTO();
        task.setName("Name2");
        taskEndpoint.loadT(session, task);
        task = new TaskDTO();
        task.setName("Name3");
        taskEndpoint.loadT(session, task);
        @Nullable final List<TaskDTO> list = taskEndpoint.findAllT(session);
        assertNotNull(list);
        assertEquals("size error", 3, list.size());
    }

    @Test
    public void testTaskRemove() throws Exception_Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskEndpoint.loadT(session, task);
        taskEndpoint.removeT(session, task.getId());
        @Nullable final TaskDTO testTask = taskEndpoint.findOneT(session, task.getId());
        assertNull(testTask);
    }

    @Test(expected = Exception_Exception.class)
    public void testTaskCreateWithDeadSession() throws Exception_Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        @Nullable final SessionDTO testSession = sessionEndpoint.getSession("testUser", "123456");
        sessionEndpoint.killSession(testSession);
        taskEndpoint.loadT(testSession, task);
    }

    @Test(expected = Exception_Exception.class)
    public void testTaskFindWithDeadSession() throws Exception_Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        @Nullable final SessionDTO testSession = sessionEndpoint.getSession("testUser", "123456");
        taskEndpoint.loadT(testSession, task);
        sessionEndpoint.killSession(testSession);
        @Nullable final TaskDTO testTask = taskEndpoint.findOneT(testSession, task.getId());
    }

    @Test(expected = Exception_Exception.class)
    public void testTaskRemoveWithDeadSession() throws Exception_Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        @Nullable final SessionDTO testSession = sessionEndpoint.getSession("testUser", "123456");
        taskEndpoint.loadT(testSession, task);
        sessionEndpoint.killSession(testSession);
        taskEndpoint.removeT(testSession, task.getId());
    }

    @Test
    public void testTaskFindWithWrongUser() throws Exception_Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskEndpoint.loadT(session, task);
        @Nullable final TaskDTO testTask = taskEndpoint.findOneT(testSession, task.getId());
        assertNull(testTask);
    }

    @Test
    public void testTaskRemoveWithWrongUser() throws Exception_Exception {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        taskEndpoint.loadT(session, task);
        taskEndpoint.removeT(testSession, task.getId());
        assertNotNull(taskEndpoint.findOneT(session, task.getId()));
    }

    private void checkEquals(@Nullable final TaskDTO testTask, @Nullable final TaskDTO task){
        assertNotNull(testTask);
        assertNotNull(task);
        assertEquals("not equals id", task.getId(), testTask.getId());
        assertEquals("not equals userId", session.getUserId(), testTask.getUserId());
        assertEquals("not equals Name", task.getName(), testTask.getName());
        assertEquals("not equals desc", task.getDescription(), testTask.getDescription());
        assertEquals("not equals createDate", DateUtil.toDate(task.getCreateDate()).toString(), DateUtil.toDate(testTask.getCreateDate()).toString());
        assertEquals("not equals startDate", DateUtil.toDate(task.getStartDate()).toString(), DateUtil.toDate(testTask.getStartDate()).toString());
        assertEquals("not equals finishDate", DateUtil.toDate(task.getFinishDate()).toString(), DateUtil.toDate(testTask.getFinishDate()).toString());
        assertEquals("not equals status", task.getStatus(), testTask.getStatus());
    }
}