package ru.buzanov.tm;

import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.buzanov.tm.endpoint.*;

import java.lang.Exception;
import java.net.URL;

@Category(IntagratedTests.class)
public class SessionEndpointTest extends Assert {
    @Nullable
    private UserEndpoint userEndpoint;
    @Nullable
    private SessionEndpoint sessionEndpoint;

    @Before
    public void setUp() throws Exception {
        this.userEndpoint = new UserEndpointService(new URL("http://localhost:8080/UserEndpoint?wsdl")).getUserEndpointPort();
        this.sessionEndpoint = new SessionEndpointService(new URL("http://localhost:8080/SessionEndpoint?wsdl")).getSessionEndpointPort();
        userEndpoint.registryUser("testUser", "123456");
        assertNotNull(userEndpoint);
        assertNotNull(sessionEndpoint);
    }

    @After
    public void tearDown() throws Exception {
        userEndpoint.remove(sessionEndpoint.getSession("testUser", "123456"));
    }

    @Test
    public void testSessionGetRight() throws Exception_Exception {
        @Nullable final SessionDTO session = sessionEndpoint.getSession("testUser", "123456");
        assertNotNull(session);
        assertNotNull(session.getId());
        assertNotNull(session.getCreateDate());
        assertNotNull(session.getSignature());
        assertNotNull(session.getUserId());
    }

    @Test(expected = Exception_Exception.class)
    public void testSessionGetWrong() throws Exception_Exception {
        @Nullable final SessionDTO session = sessionEndpoint.getSession("testUser", "321321");
        assertNull(session);
    }

    @Test
    public void testSessionUpdate() throws Exception_Exception, InterruptedException {
        @Nullable final SessionDTO session = sessionEndpoint.getSession("testUser", "123456");
        assertNotNull(session);
        Thread.sleep(1000);
        @Nullable final SessionDTO testSession = sessionEndpoint.updateSession(session);
        assertNotNull(testSession);
        assertEquals(session.getUserId(), testSession.getUserId());
        assertEquals(session.getId(), testSession.getId());
        assertNotEquals(session.getSignature(), testSession.getSignature());
        assertTrue(session.getCreateDate().toGregorianCalendar().before(testSession.getCreateDate().toGregorianCalendar()));
    }

    @Test(expected = Exception_Exception.class)
    public void testSessionUpdateChanged() throws Exception_Exception {
        @Nullable final SessionDTO session = sessionEndpoint.getSession("testUser", "123456");
        assertNotNull(session);
        session.setUserId(new UserDTO().getId());
        @Nullable final SessionDTO testSession = sessionEndpoint.updateSession(session);
        assertNull(testSession);
    }

    @Test(expected = Exception_Exception.class)
    public void testSessionKillBeforeUpdate() throws Exception_Exception {
        @Nullable final SessionDTO session = sessionEndpoint.getSession("testUser", "123456");
        assertNotNull(session);
        sessionEndpoint.killSession(session);
        @Nullable final SessionDTO testSession = sessionEndpoint.updateSession(session);
        assertNull(testSession);
    }

    @Test
    public void testSessionMulty() throws Exception_Exception {
        @Nullable final SessionDTO session = sessionEndpoint.getSession("testUser", "123456");
        assertNotNull(session);
        @Nullable final SessionDTO session2 = sessionEndpoint.getSession("testUser", "123456");
        assertNotNull(session2);
        @Nullable final SessionDTO session3 = sessionEndpoint.getSession("testUser", "123456");
        assertNotNull(session3);
        @Nullable final SessionDTO session4 = sessionEndpoint.getSession("testUser", "123456");
        assertNotNull(session4);
        @Nullable final SessionDTO session5 = sessionEndpoint.getSession("testUser", "123456");
        assertNotNull(session5);
    }
}
