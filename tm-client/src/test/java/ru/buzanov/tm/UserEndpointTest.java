package ru.buzanov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.buzanov.tm.endpoint.*;

import java.lang.Exception;
import java.net.URL;

@Category(IntagratedTests.class)
public class UserEndpointTest extends Assert {
    @Nullable
    private ProjectEndpoint projectEndpoint;
    @Nullable
    private SessionDTO session;
    @Nullable
    private SessionDTO testSession;
    @Nullable
    private UserEndpoint userEndpoint;
    @Nullable
    private SessionEndpoint sessionEndpoint;
    @Nullable
    private TaskEndpoint taskEndpoint;

    @Before
    public void setUp() throws Exception {
        this.projectEndpoint = new ProjectEndpointService(new URL("http://localhost:8080/ProjectEndpoint?wsdl")).getProjectEndpointPort();
        this.userEndpoint = new UserEndpointService(new URL("http://localhost:8080/UserEndpoint?wsdl")).getUserEndpointPort();
        this.sessionEndpoint = new SessionEndpointService(new URL("http://localhost:8080/SessionEndpoint?wsdl")).getSessionEndpointPort();
        this.taskEndpoint = new TaskEndpointService(new URL("http://localhost:8080/TaskEndpoint?wsdl")).getTaskEndpointPort();
        userEndpoint.registryUser("testUser", "123456");
        userEndpoint.registryUser("testUser2","123456");
        this.session = sessionEndpoint.getSession("testUser", "123456");
        this.testSession = sessionEndpoint.getSession("testUser2", "123456");
        assertNotNull(session);
        assertNotNull(projectEndpoint);
        assertNotNull(userEndpoint);
        assertNotNull(sessionEndpoint);
        assertNotNull(taskEndpoint);
    }

    @After
    public void tearDown() throws Exception {
        userEndpoint.remove(session);
        userEndpoint.remove(testSession);
    }

    @Test(expected = Exception_Exception.class)
    public void testUserExistLogin() throws Exception_Exception {
        userEndpoint.registryUser("testUser", "123456");
    }

    @Test
    public void testUserMerge() throws Exception_Exception {
        @NotNull final UserDTO user = new UserDTO();
        user.setPasswordHash("321321");
        user.setName("testName");
        user.setLogin("testUser3");
        user.setId(userEndpoint.findOne(testSession).getId());
        userEndpoint.merge(testSession, user);
        @NotNull final UserDTO testUser = userEndpoint.findOne(testSession);
        assertEquals(user.getLogin(), testUser.getLogin());
        assertEquals(user.getName(), testUser.getName());
        assertEquals(user.getId(), testUser.getId());
        testSession = sessionEndpoint.getSession(testUser.getLogin(), user.getPasswordHash());
        assertNotNull(testSession);
    }
}