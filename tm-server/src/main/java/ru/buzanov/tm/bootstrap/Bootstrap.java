package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.endpoint.AbstractEndpoint;

import javax.xml.ws.Endpoint;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Component
public final class Bootstrap {

    @NotNull
    private static final String ADRESS = "http://0.0.0.0:8080/";
    @Autowired
    private List<AbstractEndpoint> endpoints;
    @Autowired
    private Thread sessionCleanThread;

    public void start() {
        @NotNull final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        sessionCleanThread.setDaemon(true);
        sessionCleanThread.start();

        for (AbstractEndpoint endpoint : endpoints) {
            Endpoint.publish(ADRESS + endpoint.getClass().getSimpleName() + "?wsdl", endpoint);
            System.out.println(ADRESS + endpoint.getClass().getSimpleName() + "?wsdl");
        }

        System.out.println("Task manager server is running.");

        while (true) {
            try {
                if ("exit".equals(reader.readLine())) {
                    System.exit(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}