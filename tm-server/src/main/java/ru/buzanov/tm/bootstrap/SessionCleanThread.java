package ru.buzanov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.service.SessionService;

@Component
@PropertySource("classpath:session.properties")
public class SessionCleanThread extends Thread {
    @NotNull
    @Autowired
    private ISessionService sessionService;

    @Value("${lifeTime}")
    private int lifetime;

    @Override
    public void run() {
        while (true) {
            try {
                sleep(lifetime * 2);
                sessionService.removeOld();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
