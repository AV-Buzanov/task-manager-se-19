package ru.buzanov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.dto.SessionDTO;
import ru.buzanov.tm.util.SignatureUtil;

import javax.jws.WebService;

@Component
@WebService
@NoArgsConstructor
@PropertySource("classpath:session.properties")
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    protected IProjectService projectService;
    @NotNull
    @Autowired
    protected ITaskService taskService;
    @NotNull
    @Autowired
    protected IUserService userService;
    @NotNull
    @Autowired
    protected ISessionService sessionService;

    @Value("${lifeTime}")
    protected int lifetime;
    @Value("${signSalt}")
    protected String salt;
    @Value("${signCycle}")
    protected int cycle;

    protected void auth(@Nullable final SessionDTO session) throws Exception {

        if (session == null) {
            throw new Exception("null session");
        }
        long timeDif = System.currentTimeMillis() - session.getCreateDate().getTime();
        if (timeDif > lifetime) {
            sessionService.remove(session.getId());
            throw new Exception("SessionDTO time out.");
        }
        @Nullable final String signature = session.getSignature();
        session.setSignature(null);
        @Nullable final String ourSignature = SignatureUtil.sign(session,
                salt,
                cycle);
        if (signature == null || !signature.equals(ourSignature)) {
            throw new Exception("Invalid session signature.");
        }
        SessionDTO ourSession;
        if ((ourSession = sessionService.findOne(session.getId())) == null) {
            throw new Exception("SessionDTO not found.");
        }
        if (!ourSession.getSignature().equals(ourSignature))
            throw new Exception("SessionDTO signature fail.");

        System.out.print(userService.findOne(session.getUserId()).getLogin() + "  ");
        System.out.print(Thread.currentThread().getStackTrace()[2].getClassName() + "  ");
        System.out.print(Thread.currentThread().getStackTrace()[2].getMethodName());
        System.out.println();
    }
}
