package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.dto.SessionDTO;
import ru.buzanov.tm.enumerated.Field;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@Component
public class ProjectEndpoint extends AbstractEndpoint {

    @WebMethod
    @NotNull
    public Collection<ProjectDTO> findAllP(@Nullable SessionDTO session) throws Exception {
        auth(session);
        return projectService.findAll(session.getUserId());
    }

    @WebMethod
    @Nullable
    public ProjectDTO findOneP(@Nullable SessionDTO session, @Nullable String id) throws Exception {
        auth(session);
        return projectService.findOne(session.getUserId(), id);
    }

    @WebMethod
    public boolean isNameExistP(@Nullable SessionDTO session, @Nullable String name) throws Exception {
        auth(session);
        return projectService.isNameExist(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public String getListP(@Nullable SessionDTO session) throws Exception {
        auth(session);
        return projectService.getList(session.getUserId());
    }

    @WebMethod
    @Nullable
    public String getIdByCountP(@Nullable SessionDTO session, int count) throws Exception {
        auth(session);
        return projectService.getIdByCount(session.getUserId(), count);
    }

    public void mergeP(@Nullable SessionDTO session, @Nullable String id, @Nullable ProjectDTO project) throws Exception {
        auth(session);
        projectService.merge(session.getUserId(), id, project);
    }

    @WebMethod
    @Nullable
    public ProjectDTO removeP(@Nullable SessionDTO session, @Nullable String id) throws Exception {
        auth(session);
        projectService.remove(session.getUserId(), id);
        return null;
    }

    public void removeAllP(@Nullable SessionDTO session) throws Exception {
        auth(session);
        projectService.removeAll(session.getUserId());
    }

    @WebMethod
    @NotNull
    public Collection<ProjectDTO> findByDescriptionP(@Nullable SessionDTO session, @Nullable String desc) throws Exception {
        auth(session);
        return projectService.findByDescription(session.getUserId(), desc);
    }

    @WebMethod
    @NotNull
    public Collection<ProjectDTO> findByNameP(@Nullable SessionDTO session, @Nullable String name) throws Exception {
        auth(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public @NotNull Collection<ProjectDTO> findAllOrderedP(@Nullable SessionDTO session, boolean dir, @NotNull Field field) throws Exception {
        auth(session);
        return projectService.findAllOrdered(session.getUserId(), dir, field);
    }

    @WebMethod
    @Nullable
    public ProjectDTO loadP(@Nullable SessionDTO session, @Nullable ProjectDTO entity) throws Exception {
        auth(session);
        projectService.load(session.getUserId(), entity);
        return null;
    }

    @WebMethod
    public void loadListP(@Nullable SessionDTO session, List<ProjectDTO> list) throws Exception {
        auth(session);
        projectService.load(session.getUserId(), list);
    }
}
