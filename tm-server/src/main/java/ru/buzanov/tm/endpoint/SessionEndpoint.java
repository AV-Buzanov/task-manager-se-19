package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.dto.SessionDTO;
import ru.buzanov.tm.service.UserService;
import ru.buzanov.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Date;

@WebService
@Component
public class SessionEndpoint extends AbstractEndpoint {

    @WebMethod
    @Nullable
    public SessionDTO getSession(@Nullable final String login, @Nullable final String passwordHash) throws Exception {
        if (login == null || login.isEmpty() || passwordHash == null || passwordHash.isEmpty())
            throw new Exception("empty login or password");
        if (!userService.isLoginExist(login))
            throw new Exception("Login doesn't exist.");
        if (!userService.isPassCorrect(login, passwordHash))
            throw new Exception("Invalid password.");
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(userService.findByLogin(login).getId());
        @Nullable final String signature = SignatureUtil.sign(session,
                salt,
                cycle);
        session.setSignature(signature);
        System.out.println(session.getId());
        System.out.println(session.getUserId());
        System.out.println(session.getCreateDate());
        System.out.println(session.getSignature());
        sessionService.load(session);
        return session;
    }

    @WebMethod
    @Nullable
    public SessionDTO updateSession(@Nullable final SessionDTO session) throws Exception {
        auth(session);
        session.setSignature(null);
        session.setCreateDate(new Date(System.currentTimeMillis()));
        @Nullable final String signature = SignatureUtil.sign(session,
                salt,
                cycle);
        session.setSignature(signature);
        sessionService.merge(session.getId(), session);
        return session;
    }

    @WebMethod
    public void killSession(@Nullable SessionDTO session) throws Exception {
        auth(session);
        sessionService.remove(session.getId());
    }
}
