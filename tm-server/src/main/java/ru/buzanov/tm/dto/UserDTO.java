package ru.buzanov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.enumerated.RoleType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO extends AbstractDto {
    @Nullable
    private String name;
    @Nullable
    private String login;
    @Nullable
    private String passwordHash;
    @Nullable
    @Enumerated(EnumType.STRING)
    private RoleType roleType;
}
