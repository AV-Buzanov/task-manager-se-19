package ru.buzanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.buzanov.tm.api.repository.SessionRepository;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.dto.SessionDTO;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.entity.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
@PropertySource("classpath:session.properties")
public class SessionService implements ISessionService {
    @Autowired
    private SessionRepository sessionRepository;

    @Value("${lifeTime}")
    int lifetime;

    @Transactional
    public void load(@Nullable final SessionDTO session) throws Exception {
        if (session == null || session.getId() == null || session.getSignature() == null)
            throw new Exception("Argument can't be empty or null");
        sessionRepository.saveAndFlush(toEntity(session));
    }

    @Transactional
    public void load(@Nullable final List<SessionDTO> list) throws Exception {
        if (list == null)
            throw new Exception("Argument can't be empty or null");
        for (@NotNull final SessionDTO session : list)
            load(session);
    }

    public @NotNull Collection<SessionDTO> findAll() throws Exception {
        @Nullable final List<SessionDTO> list = new ArrayList<>();
        for (@NotNull final Session session : sessionRepository.findAll())
            list.add(toDTO(session));
        return list;
    }

    @Nullable
    public SessionDTO findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (sessionRepository.findById(id).isPresent())
            return toDTO(sessionRepository.findById(id).get());
        return null;
    }

    @Transactional
    public void merge(@Nullable final String id, @Nullable final SessionDTO session) throws Exception {
        if (session == null || session.getSignature() == null)
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        sessionRepository.saveAndFlush(toEntity(session));
    }

    @Transactional
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        sessionRepository.deleteById(id);
    }

    @Transactional
    public void removeOld() {
        sessionRepository.deleteAllByCreateDateLessThan(new Date(System.currentTimeMillis() - lifetime));
    }

    @Transactional
    public void removeAll() {
        sessionRepository.deleteAll();
    }

    @Nullable
    private SessionDTO toDTO(@Nullable final Session session) {
        if (session == null)
            return null;
        @NotNull final SessionDTO sessionDto = new SessionDTO();
        if (session.getId() != null)
            sessionDto.setId(session.getId());
        if (session.getSignature() != null)
            sessionDto.setSignature(session.getSignature());
        if (session.getCreateDate() != null)
            sessionDto.setCreateDate(new Date(session.getCreateDate().getTime()));
        if (session.getUser() != null)
            sessionDto.setUserId(session.getUser().getId());
        return sessionDto;
    }

    @Nullable
    private Session toEntity(@Nullable final SessionDTO session) {
        if (session == null)
            return null;
        @NotNull final Session projectEntity = new Session();
        if (session.getId() != null)
            projectEntity.setId(session.getId());
        if (session.getSignature() != null)
            projectEntity.setSignature(session.getSignature());
        if (session.getUserId() != null)
            projectEntity.setUser(new User(session.getUserId()));
        if (session.getCreateDate() != null)
            projectEntity.setCreateDate(session.getCreateDate());
        return projectEntity;
    }
}