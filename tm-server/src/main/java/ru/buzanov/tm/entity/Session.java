package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="app_session")
public class Session extends AbstractEntity {
    public Session(@NotNull String id) {
        super(id);
    }
    @Nullable
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;
    @NotNull

    private Date createDate = new Date(System.currentTimeMillis());
    @Nullable
    private String signature;
}
