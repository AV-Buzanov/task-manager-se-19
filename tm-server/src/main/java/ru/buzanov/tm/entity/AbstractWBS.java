package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractWBS extends AbstractEntity {
    public AbstractWBS(@NotNull String id) {
        super(id);
    }
    @Nullable
    private String name;
    @ManyToOne
    @JoinColumn(name="user_id")
    @Nullable
    private User user;
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private Status status = Status.PLANNED;
    @NotNull
    private Date createDate = new Date(System.currentTimeMillis());
    @Nullable
    private Date startDate;
    @Nullable
    private Date finishDate;
    @Nullable
    private String description;
}