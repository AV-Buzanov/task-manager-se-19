package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="app_user")
public class User extends AbstractEntity {
    public User(@NotNull String id) {
        super(id);
    }
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
    @NotNull
    private List<Project> projects = new ArrayList<>();
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
    @NotNull
    private List<Task> tasks = new ArrayList<>();
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
    @NotNull
    private List<Session> sessions = new ArrayList<>();
    @Nullable
    private String name;
    @Nullable
    @Column(unique = true)
    private String login;
    @Nullable
    private String passwordHash;
    @Nullable
    @Enumerated(value = EnumType.STRING)
    private RoleType roleType;
}
