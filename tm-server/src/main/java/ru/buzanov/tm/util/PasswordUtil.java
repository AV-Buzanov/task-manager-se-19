package ru.buzanov.tm.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.Nullable;

public class PasswordUtil {

    @Nullable
    public static String hashingPass(
            @Nullable final String value,
            @Nullable final String salt
    ) {
        if (value == null || salt == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < 13; i++) {
            result = DigestUtils.md5Hex(salt + result + salt);
        }
        return result;
    }
}
