package ru.buzanov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.buzanov.tm.entity.Session;

import java.util.Date;

@Repository
public interface SessionRepository extends JpaRepository<Session, String> {

    void deleteAllByCreateDateLessThan(Date date);
}