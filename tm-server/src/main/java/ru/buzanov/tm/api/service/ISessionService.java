package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.buzanov.tm.dto.SessionDTO;

import java.util.Collection;
import java.util.List;

@Service
public interface ISessionService {
    void load(@Nullable final SessionDTO session) throws Exception;

    void load(@Nullable final List<SessionDTO> list) throws Exception;

    @NotNull Collection<SessionDTO> findAll() throws Exception;

    @Nullable SessionDTO findOne(@Nullable final String id) throws Exception;

    void merge(@Nullable final String id, @Nullable final SessionDTO session) throws Exception;

    void remove(@Nullable final String id) throws Exception;

    void removeOld();

    void removeAll();
}
