package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Collection;
import java.util.List;

@Service
public interface IUserService {
    void load(@Nullable final UserDTO user) throws Exception;

    void load(@Nullable final List<UserDTO> list) throws Exception;

    @NotNull Collection<UserDTO> findAll() throws Exception;

    @Nullable UserDTO findOne(@Nullable final String id) throws Exception;

    void merge(@Nullable final String id, @Nullable final UserDTO user) throws Exception;

    void remove(@Nullable final String id) throws Exception;

    void removeAll() throws Exception;

    @Nullable UserDTO findByLogin(@Nullable final String login) throws Exception;

    @Nullable Collection<UserDTO> findByRole(@Nullable final RoleType role) throws Exception;

    boolean isPassCorrect(@Nullable final String login, @Nullable final String pass) throws Exception;

    boolean isLoginExist(@Nullable final String login) throws Exception;

    @Nullable String getList() throws Exception;

    @Nullable String getIdByCount(@Nullable final String userId, int count) throws Exception;
}
