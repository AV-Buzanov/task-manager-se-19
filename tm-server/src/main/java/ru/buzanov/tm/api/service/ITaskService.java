package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.enumerated.Field;

import java.util.Collection;
import java.util.List;

@Service
public interface ITaskService {

    void load(@Nullable final String userId, @Nullable final TaskDTO task) throws Exception;

    void load(@Nullable String userId, List<TaskDTO> list) throws Exception;

    @NotNull Collection<TaskDTO> findAll(@Nullable final String userId) throws Exception;

    @NotNull Collection<TaskDTO> findByName(@Nullable final String userId, @Nullable final String name) throws Exception;

    @NotNull Collection<TaskDTO> findByDescription(@Nullable final String userId, @Nullable final String desc) throws Exception;

    @NotNull Collection<TaskDTO> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;

    @Nullable TaskDTO findOne(@Nullable final String userId, @Nullable final String id) throws Exception;

    boolean isNameExist(@Nullable final String userId, @Nullable final String name) throws Exception;

    void merge(@Nullable final String userId, @Nullable final String id, @Nullable final TaskDTO task) throws Exception;

    void remove(@Nullable final String userId, @Nullable final String id) throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

    void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;

    @NotNull Collection<TaskDTO> findAllOrdered(@Nullable final String userId, boolean dir, @NotNull final Field field) throws Exception;

    @Nullable String getList(String userId) throws Exception;

    @Nullable String getIdByCount(@Nullable final String userId, int count) throws Exception;
}
