package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.enumerated.Field;

import java.util.Collection;
import java.util.List;

@Service
public interface IProjectService {
    void load(@Nullable final String userId, @Nullable final ProjectDTO project) throws Exception;

    void load(@Nullable final String userId, @Nullable final List<ProjectDTO> list) throws Exception;

    @NotNull Collection<ProjectDTO> findAll(@Nullable final String userId) throws Exception;

    @NotNull Collection<ProjectDTO> findByName(@Nullable final String userId, @Nullable final String name) throws Exception;

    @NotNull Collection<ProjectDTO> findByDescription(@Nullable final String userId, @Nullable final String desc) throws Exception;

    @Nullable ProjectDTO findOne(@Nullable final String userId, @Nullable final String id) throws Exception;

    boolean isNameExist(@Nullable final String userId, @Nullable final String name) throws Exception;

    void merge(@Nullable String userId, @Nullable final String id, @Nullable final ProjectDTO project) throws Exception;

    @NotNull Collection<ProjectDTO> findAllOrdered(@Nullable final String userId, boolean dir, @NotNull final Field field) throws Exception;

    void remove(@Nullable final String userId, @Nullable final String id) throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

    @Nullable String getList(String userId) throws Exception;

    @Nullable String getIdByCount(@Nullable final String userId, int count) throws Exception;
}
