package ru.buzanov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.buzanov.tm.entity.Task;

import java.util.Collection;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    boolean existsByUserIdAndName(String userId, String name);

    Task findByUserIdAndId(String userId, String id);

    Collection<Task> findAllByUserId(String userId);

    Collection<Task> findAllByUserIdAndProjectId(String userId, String projectId);

    Collection<Task> findAllByUserIdAndNameContaining(String userId, String name);

    Collection<Task> findAllByUserIdAndDescriptionContaining(String userId, String description);

    void deleteByUserIdAndId(String userId, String id);

    void deleteAllByUserId(String userId);

    void deleteAllByUserIdAndProjectId(String userId, String projectId);

    Collection<Task> findAllByUserIdOrderByDescriptionAsc(String userId);

    Collection<Task> findAllByUserIdOrderByNameAsc(String userId);

    Collection<Task> findAllByUserIdOrderByStartDateAsc(String userId);

    Collection<Task> findAllByUserIdOrderByFinishDateAsc(String userId);
}