package ru.buzanov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Collection;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    boolean existsByLogin(String login);

    User findByLogin(String login);

    Collection<User> findAllByRoleType(RoleType roleType);
}